"""
"""
from sqlalchemy import MetaData, Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base


def test_meta_data():
    metadata = MetaData()
    print(metadata.__dict__)
    return metadata


def test_meta_data_db(engine):
    metadata = MetaData(engine)
    print(metadata.__dict__)
    return metadata


def test_declarative_base(**kvargs):
    if kvargs:  
        Base = declarative_base(**kvargs)
    else:
        Base = declarative_base()
    
    class User(Base):
        __tablename__ = 'user'
        
        id   = Column(Integer, primary_key=True)
        name = Column(String)

    print(User.metadata.__dict__)


if __name__ == '__main__':
    from session import DataBase, get_engine
    
    engine = get_engine(DataBase.SQLITE)
    metadata = test_meta_data()
    test_meta_data_db(engine)
    test_declarative_base(bind=engine)
    test_declarative_base(metadata=metadata)