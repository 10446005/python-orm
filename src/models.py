# coding: utf-8
from sqlalchemy import CHAR, Column, Date, ForeignKey, Integer, String, text, Sequence
from sqlalchemy.orm import relationship, declarative_base


Base = declarative_base()


class Customer(Base):
    __tablename__ = 'customer'

    cusno = Column(CHAR(4), primary_key=True)
    cusname = Column(String(50))
    contactor = Column(String(50))
    title = Column(String(50))
    address = Column(String(255))
    city = Column(String(50))
    tel = Column(String(50))
    fax = Column(String(50))

    def __str__(self):
        return f'{self.cusno}'


class Employee(Base):
    __tablename__ = 'employee'

    empno = Column(CHAR(4), primary_key=True)
    empname = Column(String(50))
    title = Column(String(50))
    birthday = Column(Date)
    address = Column(String(255))
    city = Column(String(50))
    tel = Column(String(50))
    ext = Column(String(50), comment='分機')
    password = Column(String(50))
    base = Column(Integer, comment='底薪')


class Protype(Base):
    __tablename__ = 'protype'

    typno = Column(CHAR(4), primary_key=True)
    typename = Column(String(50))
    details = Column(String(255))


class Supplier(Base):
    __tablename__ = 'supplier'

    supno = Column(CHAR(4), primary_key=True)
    supname = Column(String(50))
    contactor = Column(String(50))
    title = Column(String(50))
    address = Column(String(255))
    city = Column(String(50))
    tel = Column(String(50))
    fax = Column(String(50))


class Ordmaster(Base):
    __tablename__ = 'ordmaster'

    ordno = Column(CHAR(5), primary_key=True)
    cusno = Column(ForeignKey('customer.cusno', ondelete='RESTRICT', onupdate='RESTRICT'))
    empno = Column(ForeignKey('employee.empno', ondelete='RESTRICT', onupdate='RESTRICT'))
    orddate = Column(Date)
    transfee = Column(Integer, comment='運費')

    customer = relationship('Customer')
    employee = relationship('Employee')


class Product(Base):
    __tablename__ = 'product'

    prono = Column(CHAR(4), primary_key=True)
    proname = Column(String(50))
    supno = Column(ForeignKey('supplier.supno', ondelete='RESTRICT', onupdate='RESTRICT'))
    typno = Column(ForeignKey('protype.typno', ondelete='RESTRICT', onupdate='RESTRICT'))
    price = Column(Integer)
    stockamt = Column(Integer)
    safeamt = Column(Integer)
    inventorydate = Column(Date)
    picture = Column(String(255))

    supplier = relationship('Supplier')
    protype = relationship('Protype')


class Orddetail(Base):
    __tablename__ = 'orddetails'

    # serno = Column(Integer, primary_key=True, server_default=text("nextval('orddetails_serno_seq'::regclass)"))
    ORDDETAILS_SERNO_SEQ = Sequence('orddetails_serno_seq')
    serno = Column(Integer, ORDDETAILS_SERNO_SEQ, primary_key=True, server_default=ORDDETAILS_SERNO_SEQ.next_value())
    ordno = Column(ForeignKey('ordmaster.ordno', ondelete='RESTRICT', onupdate='RESTRICT'))
    prono = Column(ForeignKey('product.prono', ondelete='RESTRICT', onupdate='RESTRICT'))
    amt = Column(Integer)

    ordmaster = relationship('Ordmaster')
    product = relationship('Product')
    


def recreate_models(engine):
    # To enable the “check first for the table existing” logic, add the checkfirst=True argument to create() or drop():
    models = (Customer, Employee, Protype, Supplier, Ordmaster, Product, Orddetail)
    
    for model in reversed(models):
        model.__table__.drop(engine, checkfirst=True)
        
    for model in models:
        model.__table__.create(engine, checkfirst=False)
    # Orddetail.__table__.drop(engine, checkfirst=True)
    # Orddetail.__table__.create(engine, checkfirst=False)


def recreate_model_all(engine):
    Base.metadata.drop_all(engine, checkfirst=True)
    Base.metadata.create_all(engine, checkfirst=False)


if __name__ == '__main__':
    from session import DataBase, get_engine
    engine = get_engine(DataBase.POSTGRES)
    recreate_models(engine)
    recreate_model_all(engine)