from sqlalchemy import select, update
from sqlalchemy.engine import Engine, Result, Connection
from sqlalchemy.orm import Session

from models import (
    Customer,
    Employee,
    Protype,
    Supplier,
    Ordmaster,
    Product,
    Orddetail,
)


def all_customers(engine: Engine):
    with engine.connect() as connection:  # type: Connection
        # Connection, SQL API
        customers_query = select(Customer).limit(5)
        customers_pks = connection.execute(
            customers_query
        ).scalars()
        # SELECT
        # cusno, cusname, contactor, title, address, city, tel, fax
        # FROM customer
        # scalars(index=0) get customer.cusno

        for cusno in customers_pks:
            print(cusno)

        customers_list = connection.execute(
            customers_query
        ).all()

        for customer in customers_list:
            # return list of `Customer` Row
            # Row like `collections.namedtuple`
            print(customer)
            print(customer.title)


def get_customer(engine: Engine, pk: str):
    # https://docs.sqlalchemy.org/en/14/orm/session_basics.html
    # Session, ORM API
    with Session(engine) as session:
        customer = session.get(Customer, pk)
        customer: Customer
        print(customer)
        print(customer.cusname)


def create_customer(engine: Engine, customer: Customer):
    with Session(engine, expire_on_commit=False) as session:
        session.add(customer)
        session.commit()
    return customer


def update_customer(engine: Engine, customer: Customer, **update_data):
    with Session(engine, expire_on_commit=False) as session:
        session.execute(
            update(Customer).
            where(Customer.cusno == customer.cusno).
            values(**update_data)
        )
        session.commit()


def update_customer_back(engine: Engine, customer: Customer, **update_data):
    with Session(engine, expire_on_commit=False) as session:
        # Eq == customer.title = '....'
        for attr, value in update_data.items():
            if hasattr(customer, attr):
                setattr(customer, attr, value)
        session.commit()
    return customer


def delete_customer(engine: Engine, customer: Customer):
    with Session(engine, expire_on_commit=False) as session:
        session.delete(customer)
        session.commit()
        return True


if __name__ == '__main__':
    from session import DataBase, get_engine

    engine = get_engine(DataBase.POSTGRES)
    data = {
        'cusno': 'C001',
        'cusname': '三川實業有限公司',
        'contactor': '陳小姐',
        'title': '業務',
        'address': '台北市忠孝東路四段32號',
        'city': '台北市',
        'tel': '(02) 968-9652',
        'fax': '(02) 968-9651',
    }
    customer = Customer(**data)
    customer = create_customer(engine, customer)
    all_customers(engine)
    get_customer(engine, customer.cusno)
    customer = update_customer_back(
        engine,
        customer,
        title='業務主管',
    )
    print(customer.title)
    delete_customer(engine, customer)
