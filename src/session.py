# https://docs.sqlalchemy.org/en/14/core/engines.html
# https://sanyuesha.com/2019/01/02/sqlalchemy-pool-mechanism/
"""
create_engine(
    echo=Logging SQL
    future=Use sqlalchemy v2 DBAPI 
)
"""
from enum import Enum

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL


def create_in_memory_sqlite():
    DB_URL = 'sqlite+pysqlite:///:memory:'
    engine = create_engine(DB_URL, echo=True, future=True)
    
    return engine

def create_sqlite():
    DB_URL = 'sqlite+pysqlite:///sqlite.db'
    engine = create_engine(DB_URL, echo=True, future=True)
    
    return engine

def create_postgresql():
    DB_URL = 'postgresql+psycopg2://server:pythonuser1234@127.0.0.1:5432/test'
    engine = create_engine(DB_URL, echo=True, future=True, encoding='utf-8', pool_size=20, max_overflow=10)
    
    return engine


def create_mysql():
    MYSQL_CONFIG = {
        'drivername': 'mysql+mysqldb',
        'username': 'server',
        'password': 'pythonuser1234',
        'host': '127.0.0.1',
        'port': 3306,
        'database': 'test',
        'query': {
            'charset': 'utf8mb4',
        },
    }
    # DB_URL = 'mysql+mysqldb://server:pythonuser1234@127.0.0.1:3306/test?charset=utf8mb4'
    DB_URL = URL(**MYSQL_CONFIG)
    engine = create_engine(DB_URL, echo=True, future=True, encoding='utf-8', pool_size=20, max_overflow=10)
    
    return engine


class DataBase(Enum):
    SQLITE_MEMORY = create_in_memory_sqlite
    SQLITE = create_sqlite
    POSTGRES = create_postgresql
    MYSQL = create_mysql


def get_engine(database: DataBase):
    return database()


if __name__ == '__main__':
    
    from sqlalchemy import text
    
    _session = create_in_memory_sqlite()
    with _session.connect() as connection:
        # SQL 語法帶入實際參數, 使用 sqlalchemy 使用替換變數，另外將參數帶入，
        # 防止SQL注入攻擊，並且讓sqlalchemy 將 Python type 轉成 SQL type
        result = connection.execute(
                text("SELECT :msg"),
                {'msg': 'Hello World'},
            )
        # Not use `conn.commit()`, then conn.rollback()
        print(result.all())